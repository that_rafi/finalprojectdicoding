package com.rafi.finalproject.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rafi.finalproject.R;
import com.rafi.finalproject.entity.EntertainmentEntity;

import java.util.ArrayList;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder> {
    ArrayList<EntertainmentEntity> entlist = new ArrayList<>();
    Context context;
    Activity activity;

    public FavoriteAdapter(Activity activity) {
        this.activity = activity;
    }

    public FavoriteAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<EntertainmentEntity> getEntlist() {
        return entlist;
    }

    public void setEntlist(ArrayList<EntertainmentEntity> entlist) {

            this.entlist.clear();
            this.entlist.addAll(entlist);
            notifyDataSetChanged();

    }

    // crud

    public void addItem(EntertainmentEntity entity){
        this.entlist.add(entity);
        notifyItemInserted(entlist.size()-1);
    }

    public void removeItem(int pos){
        this.entlist.remove(pos);
        notifyItemRemoved(pos);
        notifyItemRangeChanged(pos,entlist.size());
    }

    @NonNull
    @Override
    public FavoriteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_entertainment,parent,false);
        return new FavoriteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteViewHolder holder, int position) {
        holder.title.setText(entlist.get(position).getTitle());
        holder.desc.setText(entlist.get(position).getDesc());
        holder.date.setText(entlist.get(position).getDate());
        Glide.with(activity).load("https://image.tmdb.org/t/p/w500/"+entlist.get(position).getImg()).into(holder.img);
        holder.ratbar.setRating(entlist.get(position).getRating());
    }

    @Override
    public int getItemCount() {
        return entlist.size();
    }

    public class FavoriteViewHolder extends RecyclerView.ViewHolder {
        TextView title,desc,date;
        ImageView img;
        RatingBar ratbar;
        public FavoriteViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            desc = itemView.findViewById(R.id.descdet);
            date = itemView.findViewById(R.id.date);
            img = itemView.findViewById(R.id.imageView);
            ratbar = itemView.findViewById(R.id.ratingBar);
        }
    }
}
