package com.rafi.finalproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rafi.finalproject.entity.EntertainmentEntity;

public class DetailFavActivity extends AppCompatActivity {
    public final static String DETAIL_FAV= "detfav";
    private EntertainmentEntity entertainment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_fav);
        TextView title,date,desc;
        ImageView img;
        RatingBar ratingBar;

        title = findViewById(R.id.titledetfav);
        date = findViewById(R.id.datefav);
        desc = findViewById(R.id.descdetfav);
        img = findViewById(R.id.imageView2fav);
        ratingBar = findViewById(R.id.ratdetfav);

        entertainment = getIntent().getParcelableExtra(DETAIL_FAV);
        title.setText(entertainment.getTitle());
        date.setText(entertainment.getDate());
        desc.setText(entertainment.getDesc());
        Glide.with(this).load("https://image.tmdb.org/t/p/w500/"+entertainment.getImg()).into(img);
        ratingBar.setRating(entertainment.getRating());

    }
}
