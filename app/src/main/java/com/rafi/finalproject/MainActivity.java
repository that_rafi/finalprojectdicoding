package com.rafi.finalproject;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.rafi.finalproject.db.EntertainmentHelper;
import com.rafi.finalproject.ui.main.SectionsPagerAdapter;

import java.util.Locale;

import static com.rafi.finalproject.ReminderActivity.Rem_lang;

// Muhammad Rafiudin
public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    public static final String LANG = "lang";
    public static final String KEY = "key";
    public static final String FLAG_KEY = "flag";
    private String lang = "";
    private SectionsPagerAdapter sectionsPagerAdapter;
    private ViewPager viewPager;
    private  TabLayout tabs;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lang = getIntent().getStringExtra(LANG);
        if (lang != null) {
            lang = getIntent().getStringExtra(LANG);
        } else {
            lang = "eng";
        }
        String key = getIntent().getStringExtra(KEY);
        if(key == null){
            key="";
        }
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_main);
        viewPager = findViewById(R.id.view_pager);
        tabs = findViewById(R.id.tabs);
        sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager(), lang,key);
        viewPager.setAdapter(sectionsPagerAdapter);
        tabs.setupWithViewPager(viewPager);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        int flag = getIntent().getIntExtra(FLAG_KEY,0);
        if(flag ==1){
            getSupportActionBar().setTitle("Search Result");
        }
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,FavoriteActivity.class);
                startActivity(intent);
            }
        });

    }

    // create option menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optionmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_change_setting) {
            Intent intent = new Intent(MainActivity.this, SettingActivity.class);
            startActivity(intent);
            finish();

        }else if(item.getItemId() == R.id.reminder){
            Intent intent = new Intent(MainActivity.this,ReminderActivity.class);
            intent.putExtra(Rem_lang,lang);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Intent intent = new Intent(MainActivity.this,MainActivity.class);
        intent.putExtra(KEY,query);
        intent.putExtra(FLAG_KEY,1);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return true;
    }



}