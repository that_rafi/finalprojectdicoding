package com.rafi.finalproject;

import android.database.Cursor;

public interface LoadFavCallBack {
    void preExecute();
    void postExecute(Cursor cursor, Boolean flag);
}
