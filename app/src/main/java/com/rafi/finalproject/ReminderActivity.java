package com.rafi.finalproject;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.FragmentTransaction;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.rafi.finalproject.adapter.EntertainmentAdapter;
import com.rafi.finalproject.db.EntertainmentHelper;
import com.rafi.finalproject.entity.ReminderEntity;
import com.rafi.finalproject.model.Entertainment;
import com.rafi.finalproject.model.EntertainmentItem;
import com.rafi.finalproject.network.ApiReminderInterface;
import com.rafi.finalproject.network.ApiSearchInterface;
import com.rafi.finalproject.network.RetrofitClientInstance;
import com.rafi.finalproject.reminder.LoadRemCallback;
import com.rafi.finalproject.reminder.ReleaseReciever;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.CONTENT_URI;

public class ReminderActivity extends AppCompatActivity  {
    // view
    private String lang;
    public static final String Rem_lang = "reminder_lang";
    private Switch daily,release;
    //db

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        // language
        lang = getIntent().getStringExtra(Rem_lang);
        if (lang != null) {
            lang = getIntent().getStringExtra(Rem_lang);
        } else {
            lang = "eng";
        }
        ReminderSetting mFrag = new ReminderSetting();
        Bundle bundle = new Bundle();
        bundle.putString(ReminderSetting.FRAG_LANG, lang);   //parameters are (key, value).
        mFrag.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.reminderlayout, mFrag).commit();
    }


}
