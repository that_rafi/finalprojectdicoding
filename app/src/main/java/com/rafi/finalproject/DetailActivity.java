package com.rafi.finalproject;


import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.rafi.finalproject.adapter.EntertainmentAdapter;
import com.rafi.finalproject.entity.EntertainmentEntity;
import com.rafi.finalproject.model.Entertainment;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.CONTENT_URI;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.DATE;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.DESC;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.IMG;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.Lang;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.RAT;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.TITLE;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.TYPE;
import static com.rafi.finalproject.helper.MappingHelper.mapCursorToArrayList;


public class DetailActivity extends AppCompatActivity implements LoadEntCallback {
    public final static String DETAIL_KEY = "detail";
    public final static String DETAIL_Lang = "lang";
    public final static String FLAG = "flag";
    public static final String EXTRA_STATE = "EXTRA_STATE";
    // view
    private TextView title,date,desc;
    private ImageView img;
    private RatingBar ratingBar;

    private Entertainment entertainment;
    private EntertainmentAdapter adapter;
    private ArrayList<EntertainmentEntity> ent,enttv,entmv;
    private boolean tag = false;
    private int iddelete;
    private int flag;

    // provider
    private static HandlerThread handlerThread;
    private DataObserver myObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        title = findViewById(R.id.titledet);
        date = findViewById(R.id.date);
        desc = findViewById(R.id.descdet);
        img = findViewById(R.id.imageView2);
        ratingBar = findViewById(R.id.ratdet);
        entmv = new ArrayList<>();
        enttv = new ArrayList<>();
        // db
        /*helper = EntertainmentHelper.getInstance(getApplicationContext());
        helper.open();*/
        handlerThread = new HandlerThread("DataObserver");
        handlerThread.start();
        Handler handler = new Handler(handlerThread.getLooper());
        myObserver = new DataObserver(handler,this);
        getContentResolver().registerContentObserver(CONTENT_URI,true,myObserver);
        //------------
        adapter = new EntertainmentAdapter(this);
        //=--------------=//
        entertainment = getIntent().getParcelableExtra(DETAIL_KEY);
        flag = getIntent().getIntExtra(FLAG,0);
        if(flag == 0){
            title.setText(entertainment.getTitle_movie());
            date.setText(entertainment.getDate());
        }else if(flag ==1){
            title.setText(entertainment.getTitle_tv());
            date.setText(entertainment.getFirstair());
        }else{
            title.setText("-");
        }
        desc.setText(entertainment.getDesc());
        Glide.with(getApplicationContext()).load("https://image.tmdb.org/t/p/w500/"+entertainment.getImg()).into(img);
        ratingBar.setRating(entertainment.getRating());

        if(savedInstanceState == null){
            new LoadNotesAsync(this,this).execute();
        }else{
            ent = savedInstanceState.getParcelableArrayList(EXTRA_STATE);
            if(ent!=null){
                adapter.setEntitiy(ent);
                ContentValues values = new ContentValues();
                if(flag == 0){
                    values.put(TYPE,"Movie");
                    values.put(TITLE,entertainment.getTitle_movie());
                    values.put(DATE,entertainment.getDate());
                    // memasukan ke specific arraylist
                    if(ent.size()>0 ){
                        for(int i=0;i<ent.size();i++){
                            if(ent.get(i).getType().equals("Movie") ){
                                entmv = ent;
                            }
                        }
                        for(int i=0;i<entmv.size();i++){ // untuk cek apakah detail ini sudah di database
                            if(entmv.get(i).getImg().equals(entertainment.getImg()) ){ // kenapa image krn image tidak bisa di translate ke indo
                                tag = true;
                                iddelete = entmv.get(i).getId();
                            }
                        }
                    }
                }else if(flag == 1){
                    values.put(TYPE,"Tv");
                    values.put(TITLE,entertainment.getTitle_tv());
                    values.put(DATE,entertainment.getFirstair());
                    if(ent.size()>0){
                        for(int i=0;i<ent.size();i++){
                            if(ent.get(i).getType().equals("Tv") ){
                                enttv = ent;
                            }
                        }
                        for(int i=0;i<enttv.size();i++){ // untuk cek apakah detail ini sudah di database
                            if(enttv.get(i).getImg().equals(entertainment.getImg()) ){
                                tag = true;
                                iddelete = enttv.get(i).getId();
                            }
                        }
                    }

                }else{
                    title.setText("-");
                }

                values.put(Lang,getIntent().getStringExtra(DETAIL_Lang));
                values.put(RAT,entertainment.getRating());
                values.put(DESC,entertainment.getDesc());
                values.put(IMG,entertainment.getImg());

                FloatingActionButton fab = findViewById(R.id.fabdet);
                if(tag){
                    fab.setImageResource(R.drawable.ic_favorite_black_24dp);
                }
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(!tag){ // jika  tidak ada maka di tambahkan

                            Uri uri= getContentResolver().insert(CONTENT_URI,values);
                            if(uri != null || ! uri.equals("")){
                                Toast.makeText(DetailActivity.this,"Successfully Added",Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(DetailActivity.this,"Unsucessfully Added",Toast.LENGTH_SHORT).show();
                            }
                            fab.setImageResource(R.drawable.ic_favorite_black_24dp);
                            tag = true;
                            if(flag == 0){ // LIFO last in first out
                                if(entmv.size() > 0){
                                    iddelete = entmv.get(0).getId();
                                }else{
                                    iddelete = 1;
                                }

                            }else if(flag == 1){
                                if(enttv.size() > 0){
                                    iddelete = enttv.get(0).getId();
                                }else{
                                    iddelete = 1;
                                }
                            }
                        }else{ // jika ada maka kalo di klik bakal ngapus
                            Uri uri = Uri.parse(CONTENT_URI+"/"+iddelete);
                            int del = getContentResolver().delete(uri, null, null);
                            tag = false;
                            fab.setImageResource(R.drawable.ic_favorite_border_white_24dp);
                            if(del >0){
                                Toast.makeText(DetailActivity.this,"Successfully Deleted",Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(DetailActivity.this,"Unsucessfully Deleted",Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                });
            }

        }



    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(EXTRA_STATE,adapter.getEntitiy());
    }

    @Override
    public void preExecute() {

    }

    @Override
    public void postExecute(Cursor cursor) {

        ent = mapCursorToArrayList(cursor);
        adapter.setEntitiy(ent);
        ContentValues values = new ContentValues();
        if(flag == 0){
            values.put(TYPE,"Movie");
            values.put(TITLE,entertainment.getTitle_movie());
            values.put(DATE,entertainment.getDate());
            // memasukan ke specific arraylist
            if(ent.size()>0 ){
                for(int i=0;i<ent.size();i++){
                    if(ent.get(i).getType().equals("Movie") ){
                        entmv = ent;
                    }
                }
                for(int i=0;i<entmv.size();i++){ // untuk cek apakah detail ini sudah di database
                    if(entmv.get(i).getImg().equals(entertainment.getImg()) ){ // kenapa image krn image tidak bisa di translate ke indo
                        tag = true;
                        iddelete = entmv.get(i).getId();
                    }
                }
            }
        }else if(flag == 1){
            values.put(TYPE,"Tv");
            values.put(TITLE,entertainment.getTitle_tv());
            values.put(DATE,entertainment.getFirstair());
            if(ent.size()>0){
                for(int i=0;i<ent.size();i++){
                    if(ent.get(i).getType().equals("Tv") ){
                        enttv = ent;
                    }
                }
                for(int i=0;i<enttv.size();i++){ // untuk cek apakah detail ini sudah di database
                    if(enttv.get(i).getImg().equals(entertainment.getImg()) ){
                        tag = true;
                        iddelete = enttv.get(i).getId();
                    }
                }
            }

        }else{
            title.setText("-");
        }

        values.put(Lang,getIntent().getStringExtra(DETAIL_Lang));
        values.put(RAT,entertainment.getRating());
        values.put(DESC,entertainment.getDesc());
        values.put(IMG,entertainment.getImg());

        FloatingActionButton fab = findViewById(R.id.fabdet);
        if(tag){
            fab.setImageResource(R.drawable.ic_favorite_black_24dp);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!tag){ // jika  tidak ada maka di tambahkan

                    Uri uri= getContentResolver().insert(CONTENT_URI,values);
                    if(uri != null || ! uri.equals("")){
                        Toast.makeText(DetailActivity.this,"Successfully Added",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(DetailActivity.this,"Unsucessfully Added",Toast.LENGTH_SHORT).show();
                    }
                    fab.setImageResource(R.drawable.ic_favorite_black_24dp);
                    tag = true;
                    if(flag == 0){ // LIFO last in first out
                        if(entmv.size() > 0){
                            iddelete = entmv.get(0).getId();
                        }else{
                            iddelete = 1;
                        }

                    }else if(flag == 1){
                        if(enttv.size() > 0){
                            iddelete = enttv.get(0).getId();
                        }else{
                            iddelete = 1;
                        }
                    }
                }else{ // jika ada maka kalo di klik bakal ngapus
                    Uri uri = Uri.parse(CONTENT_URI+"/"+iddelete);
                    int del = getContentResolver().delete(uri, null, null);
                    tag = false;
                    fab.setImageResource(R.drawable.ic_favorite_border_white_24dp);
                    if(del >0){
                        Toast.makeText(DetailActivity.this,"Successfully Deleted",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(DetailActivity.this,"Unsucessfully Deleted",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }


    private static class LoadNotesAsync extends AsyncTask<Void,Void, Cursor> {
        private final WeakReference<Context> weakContext;
        private final WeakReference<LoadEntCallback> weakCallback;

        public LoadNotesAsync(Context c,LoadEntCallback Callback) {
            this.weakContext = new WeakReference<>(c);
            this.weakCallback = new WeakReference<>(Callback);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected Cursor doInBackground(Void... voids) {
            Context context= weakContext.get();
            return context.getContentResolver().query(CONTENT_URI,null,null,null,null);
        }

        @Override
        protected void onPostExecute(Cursor ents) {
            super.onPostExecute(ents);
            weakCallback.get().postExecute(ents);

        }

    }

    public static class DataObserver extends ContentObserver {
        final Context context;
        public DataObserver(Handler handler,Context context){
            super(handler);
            this.context = context;
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            new LoadNotesAsync(context,(LoadEntCallback) context).execute();
        }
    }



}
