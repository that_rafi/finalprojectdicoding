package com.rafi.finalproject;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.rafi.finalproject.adapter.FavoriteAdapter;
import com.rafi.finalproject.entity.EntertainmentEntity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.HandlerThread;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.CONTENT_URI;
import static com.rafi.finalproject.helper.MappingHelperFavorite.mapCursorToArrayList;

public class FavoriteActivity extends AppCompatActivity implements LoadFavCallBack {
    private FavoriteAdapter adapter;
    private DataObserver observer;
    private RecyclerView rv;
    public static final String EXTRA_STATE = "EXTRA_STATE";
    private ArrayList<EntertainmentEntity> ent;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    new LoadEntAsync(FavoriteActivity.this,FavoriteActivity.this,true).execute();
                    return true;
                case R.id.navigation_dashboard:
                    new LoadEntAsync(FavoriteActivity.this,FavoriteActivity.this,false).execute();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        rv = findViewById(R.id.rvfav);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setHasFixedSize(true);
        // db
        HandlerThread handlerThread = new HandlerThread("DataObserver");
        handlerThread.start();
        Handler handler = new Handler(handlerThread.getLooper());
        observer = new DataObserver(handler,this);
        getContentResolver().registerContentObserver(CONTENT_URI,true,observer);
        adapter = new FavoriteAdapter(this);
        rv.setAdapter(adapter);

        if(savedInstanceState == null){
            new LoadEntAsync(this,this,true).execute();
        }else{
            ArrayList<EntertainmentEntity> list = savedInstanceState.getParcelableArrayList(EXTRA_STATE);
            if(list!=null){
                adapter.setEntlist(list);
            }
        }
        ItemClickSupport.addTo(rv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent intent = new Intent(FavoriteActivity.this,DetailFavActivity.class);
                intent.putExtra(DetailFavActivity.DETAIL_FAV,adapter.getEntlist().get(position));
                startActivity(intent);
            }
        });
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(EXTRA_STATE,adapter.getEntlist());
    }

    @Override
    public void preExecute() {

    }

    @Override
    public void postExecute(Cursor cursor,Boolean flag) {
        ent = mapCursorToArrayList(cursor,flag);
        if(ent.size()>0){
            adapter.setEntlist(ent);
        }else{
            Toast.makeText(this,"There's no any data currently",Toast.LENGTH_SHORT).show();
            adapter.setEntlist(new ArrayList<EntertainmentEntity>());
        }
    }
    private static class LoadEntAsync extends AsyncTask<Void,Void, Cursor> {
        private final WeakReference<Context> weakContext;
        private final WeakReference<LoadFavCallBack> weakCallback;
        private  Boolean flag;

        public LoadEntAsync(Context context,LoadFavCallBack Callback,Boolean f) {
            this.weakContext = new WeakReference<>(context);
            this.weakCallback = new WeakReference<>(Callback);
            this.flag = f;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            super.onPostExecute(cursor);
            weakCallback.get().postExecute(cursor,flag);
        }

        @Override
        protected Cursor doInBackground(Void... voids) {
            return weakContext.get().getContentResolver().query(CONTENT_URI,null,null,null,null);
        }
    }

    // observer

    static class DataObserver extends ContentObserver {
        final Context context;
        public DataObserver(Handler handler,Context context) {
            super(handler);
            this.context = context;
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            new LoadEntAsync(context,(LoadFavCallBack) context,true).execute();
        }
    }
}
