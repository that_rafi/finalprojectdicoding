package com.rafi.finalproject.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.rafi.finalproject.entity.EntertainmentEntity;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.DATE;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.DESC;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.IMG;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.Lang;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.RAT;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.TITLE;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.TYPE;
import static com.rafi.finalproject.db.DatabaseContract.TABLE_NOTE;

public class EntertainmentHelper {
    public static final String DATABASE_TABLE = TABLE_NOTE;
    private static DatabaseHelper databaseHelper;
    private static EntertainmentHelper INSTANCE;
    private static SQLiteDatabase db;

    private EntertainmentHelper(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }
    public static EntertainmentHelper getInstance(Context context){
        if(INSTANCE == null){
            synchronized (SQLiteOpenHelper.class){
                if(INSTANCE == null){
                    INSTANCE = new EntertainmentHelper(context);
                }
            }
        }
        return INSTANCE;
    }
    public void open() throws SQLException{
        db = databaseHelper.getWritableDatabase();
    }
    public  void close(){
        databaseHelper.close();
        if(db.isOpen())
            db.close();
    }
    // CRUD
    // READ
    public ArrayList<EntertainmentEntity> getAllNotes(boolean flag){
        ArrayList<EntertainmentEntity> array = new ArrayList<>();
        String type = "Movie";
        if(!flag){
            type ="Tv";
        }
        Cursor cursor = db.query(DATABASE_TABLE,null,
                TYPE+" =?",
                new String[]{type},
                null,
                null,
                _ID +" DESC",
                null
                );
        cursor.moveToFirst();
        EntertainmentEntity ent;
        if (cursor.getCount()>0){
            do{
                ent = new EntertainmentEntity();
                ent.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                ent.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                ent.setDesc(cursor.getString(cursor.getColumnIndexOrThrow(DESC)));
                ent.setDate(cursor.getString(cursor.getColumnIndexOrThrow(DATE)));
                ent.setRating(cursor.getFloat(cursor.getColumnIndexOrThrow(RAT)));
                ent.setLang(cursor.getString(cursor.getColumnIndexOrThrow(Lang)));
                ent.setImg(cursor.getString(cursor.getColumnIndexOrThrow(IMG)));
                ent.setType(cursor.getString(cursor.getColumnIndexOrThrow(TYPE)));
                array.add(ent);
                cursor.moveToNext();
            }while (!cursor.isAfterLast());
        }
        cursor.close();
        return array;
    }

    // content provider

    public Cursor queryByIdProvider(String id){
        return db.query(DATABASE_TABLE,null
                ,_ID+" =?"
                ,new String[]{id}
                ,null
                ,null
                ,null
                ,null);

    }

    public Cursor queryProvider(){
        return db.query(DATABASE_TABLE
                , null
                , null
                , null
                , null
                , null
                , _ID + " ASC");
    }

    public long insertProvider(ContentValues values){
        return db.insert(DATABASE_TABLE,null,values);
    }

    public int updateProvider(String id,ContentValues values){
        return db.update(DATABASE_TABLE,values,_ID+" =?",new String[]{id});
    }
    public int deleteProvider(String id){
        return db.delete(DATABASE_TABLE,_ID+" = ?",new String[]{id});
    }

    public int deleteall(){
        return db.delete(TABLE_NOTE,null,null);
    }

}
