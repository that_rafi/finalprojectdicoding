package com.rafi.finalproject.db;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {
    public static final String AUTHORITY = "com.rafi.finalproject";
    public static final String SCHEME = "content";

    private DatabaseContract(){}
    public static String TABLE_NOTE = "entertainment";
    public static final class EntertainmentColumns implements BaseColumns{
        public static String RAT = "rating";
        public static String DATE = "date";
        public static String TITLE = "title";
        public static String TYPE = "type";
        public static String Lang = "lang";
        public static String IMG = "img";
        public static String DESC = "desc";

        public static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_NOTE)
                .build();
    }

    public static String getColumnString(Cursor cursor, String columnName){
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public static int getColumnInt(Cursor cursor,String columnName){
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }
    public static long getColumnLong(Cursor cursor,String columnName){
        return cursor.getLong(cursor.getColumnIndex(columnName));
    }
}
