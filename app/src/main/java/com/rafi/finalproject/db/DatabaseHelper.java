package com.rafi.finalproject.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static  String  DATABASE_NAME = "dbentertainmentapp";
    private static final int DATABASE_VERSION =4;
    private static final String SQL_CREATE_TABLE_NOTE = String.format("CREATE TABLE %s"
    + "(%s INTEGER PRIMARY KEY AUTOINCREMENT,"+
            "%s FLOAT(8,4) NOT NULL ,"+
            "%s TEXT NOT NULL,"+
            "%s TEXT NOT NULL,"+
            "%s TEXT NOT NULL,"+
            "%s TEXT NOT NULL,"+
            "%s TEXT NOT NULL,"+
            "%s TEXT NOT NULL)",DatabaseContract.TABLE_NOTE,
            DatabaseContract.EntertainmentColumns._ID,
            DatabaseContract.EntertainmentColumns.RAT,
            DatabaseContract.EntertainmentColumns.DATE,
            DatabaseContract.EntertainmentColumns.TITLE,
            DatabaseContract.EntertainmentColumns.TYPE,
            DatabaseContract.EntertainmentColumns.Lang,
            DatabaseContract.EntertainmentColumns.IMG,
            DatabaseContract.EntertainmentColumns.DESC
    );


    public DatabaseHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_NOTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+DatabaseContract.TABLE_NOTE);
        onCreate(sqLiteDatabase);
    }
}
