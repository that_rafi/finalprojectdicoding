package com.rafi.finalproject;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;

import com.google.firebase.messaging.FirebaseMessaging;
import com.rafi.finalproject.reminder.DailyReceiver;
import com.rafi.finalproject.reminder.ReleaseReciever;

import java.util.Calendar;
import java.util.Locale;

import static android.content.Context.ALARM_SERVICE;

public class ReminderSetting extends PreferenceFragmentCompat {

    public static final String TAG = "ReminderSetting";
    public static final String FRAG_LANG = "fragmentlang";

    private String lang;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);

        Bundle bundle = getArguments();
        lang = bundle.getString(FRAG_LANG);

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getActivity().getBaseContext().getResources().updateConfiguration(config,
                getActivity().getBaseContext().getResources().getDisplayMetrics());

        SwitchPreference daily = (SwitchPreference) findPreference(getResources().getString(R.string.switch_daily));
        SwitchPreference release = (SwitchPreference) findPreference(getResources().getString(R.string.switch_release));

        release.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean checked = (boolean) newValue;
                if(checked){
                    onReminder(true,8,1);
                }else{
                    cancelAlarm(true);
                    String msg = getString(R.string.releasereminderoff);
                    Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        daily.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean checked = (boolean) newValue;
                if(checked){
                    onReminder(false,7,1);
                }else{
                    cancelAlarm(false);
                    String msg = getString(R.string.dailyreminderoff);
                    Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        /*if(daily.isChecked()){
            onReminder(false,9,15);
        }else{
            cancelAlarm(false);
        }

        if (release.isChecked()) {
            onReminder(true, 9, 14);
        }else {
            cancelAlarm(true);
        }*/

        /* button listener
        daily.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    // reminder
                    // Notification
                    if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
                        String channelid = getString(R.string.default_notification_channel_id);
                        String channelname = getString(R.string.default_notification_channel_name);
                        NotificationManager notificationManager = getActivity().getSystemService(NotificationManager.class);
                        notificationManager.createNotificationChannel(new NotificationChannel(channelid,channelname,NotificationManager.IMPORTANCE_LOW));

                    }
                    if(getActivity().getIntent().getExtras()!=null){
                        for (String key : getActivity().getIntent().getExtras().keySet()){
                            Object value = getActivity().getIntent().getExtras().get(key);
                            Log.d(TAG,"Key: "+key+" Value: "+value);
                        }
                    }
                    FirebaseMessaging.getInstance().subscribeToTopic("daily");
                    String msg = getString(R.string.dailyreminderon);
                    Log.d(TAG,msg);
                    Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
                }else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic("daily");
                    String msg = getString(R.string.dailyreminderoff);
                    Log.d(TAG,msg);
                    Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
                }
            }
        });
        */

    }


    public void cancelAlarm(boolean flag){
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        Intent intent;
        if(flag){
             intent= new Intent(getActivity().getApplicationContext(), ReleaseReciever.class);
        }else{
             intent = new Intent(getActivity().getApplicationContext(), DailyReceiver.class);
        }
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 0, intent, 0);
        pendingIntent.cancel();
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
    }

    public void onReminder(boolean flag,int hour,int m){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,hour);
        calendar.set(Calendar.MINUTE,m);
        Intent intent;
        String msg;
        if(flag){
            intent= new Intent(getContext(), ReleaseReciever.class);
            msg= getString(R.string.releasereminderon);
        }else{
             msg= getString(R.string.dailyreminderon);
            intent= new Intent(getContext(), DailyReceiver.class);
        }
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(),0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager)getActivity().getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),alarmManager.INTERVAL_DAY,pendingIntent);
        Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
    }
}

