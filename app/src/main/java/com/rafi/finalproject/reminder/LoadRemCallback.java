package com.rafi.finalproject.reminder;

import com.rafi.finalproject.model.EntertainmentItem;

import java.util.List;

public interface LoadRemCallback {
    void preExecute();
    void postExecute(List<EntertainmentItem> ent);
}
