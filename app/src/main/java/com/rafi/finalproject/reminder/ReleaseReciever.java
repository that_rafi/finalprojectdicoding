package com.rafi.finalproject.reminder;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.rafi.finalproject.MainActivity;
import com.rafi.finalproject.R;
import com.rafi.finalproject.adapter.EntertainmentAdapter;
import com.rafi.finalproject.model.Entertainment;
import com.rafi.finalproject.model.EntertainmentItem;
import com.rafi.finalproject.network.ApiReminderInterface;
import com.rafi.finalproject.network.RetrofitClientInstance;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReleaseReciever extends BroadcastReceiver {

    public static final int NOTIFICATION_ID =1 ;
    public static String CHANNEL_ID = "channel_01";
    public static CharSequence CHANNEL_NAME = "dicoding channel";

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent1 = new Intent(context, MainActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //if we want ring on notifcation then uncomment below line//
        //        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        // content
        ApiReminderInterface service = RetrofitClientInstance.getRetrofitInstance().create(ApiReminderInterface.class);
        Call<EntertainmentItem> call;
        call = service.getReminder("movie",getCurrentDate(),getCurrentDate());
        call.enqueue(new Callback<EntertainmentItem>() {
            @Override
            public void onResponse(Call<EntertainmentItem> call, Response<EntertainmentItem> response) {
                List<Entertainment> data = response.body().getItems();
                PendingIntent pendingIntent = PendingIntent.getActivity(context,NOTIFICATION_ID,intent1,PendingIntent.FLAG_UPDATE_CURRENT);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context,CHANNEL_ID).
                        setSmallIcon(R.drawable.ic_notifications24dp).
                        setContentIntent(pendingIntent).
                        setContentText(  data.get(0).getTitle_reminder()+ " and many more are Realease Today!!!!").
                        setContentTitle("Entertainment").
//                setSound(alarmSound).
        setAutoCancel(true);

                if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
                    NotificationChannel channel = new NotificationChannel(CHANNEL_ID,CHANNEL_NAME,NotificationManager.IMPORTANCE_DEFAULT);

                    builder.setChannelId(CHANNEL_ID);
                    if(notificationManager != null){
                        notificationManager.createNotificationChannel(channel);
                    }
                }

                Notification notification = builder.build();

                if(notificationManager != null){
                    notificationManager.notify(NOTIFICATION_ID,notification);
                }

            }
            @Override
            public void onFailure(Call<EntertainmentItem> call, Throwable t) {
                Log.d("onFailure",t.getMessage());
            }
        });

    }

    private String getCurrentDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


}
