package com.rafi.finalproject.reminder;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.rafi.finalproject.MainActivity;
import com.rafi.finalproject.R;
import com.rafi.finalproject.model.Entertainment;
import com.rafi.finalproject.model.EntertainmentItem;
import com.rafi.finalproject.network.ApiReminderInterface;
import com.rafi.finalproject.network.RetrofitClientInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DailyReceiver extends BroadcastReceiver {
    public static final int NOTIFICATION_ID =2 ;
    public static String CHANNEL_ID = "channel_02";
    public static CharSequence CHANNEL_NAME = "rafi channel";
    @Override
    public void onReceive(Context context, Intent intent) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Intent intent1 = new Intent(context, MainActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //if we want ring on notifcation then uncomment below line//
            //        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            // content
                PendingIntent pendingIntent = PendingIntent.getActivity(context,NOTIFICATION_ID,intent1,PendingIntent.FLAG_UPDATE_CURRENT);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context,CHANNEL_ID).
                        setSmallIcon(R.drawable.ic_notifications24dp).
                        setContentIntent(pendingIntent).
                        setContentText( "Feeling boread? Cmon watch our movies and shows!!!!").
                        setContentTitle("Entertainment").
        //                setSound(alarmSound).
                setAutoCancel(true);

                if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
                    NotificationChannel channel = new NotificationChannel(CHANNEL_ID,CHANNEL_NAME,NotificationManager.IMPORTANCE_DEFAULT);

                    builder.setChannelId(CHANNEL_ID);
                    if(notificationManager != null){
                        notificationManager.createNotificationChannel(channel);
                    }
                }

                Notification notification = builder.build();

                if(notificationManager != null){
                    notificationManager.notify(NOTIFICATION_ID,notification);
                }


    }
}
