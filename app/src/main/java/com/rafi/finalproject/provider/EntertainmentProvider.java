package com.rafi.finalproject.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

import com.rafi.finalproject.DetailActivity;
import com.rafi.finalproject.db.EntertainmentHelper;
import static com.rafi.finalproject.db.DatabaseContract.AUTHORITY;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.CONTENT_URI;
import static com.rafi.finalproject.db.DatabaseContract.TABLE_NOTE;

public class EntertainmentProvider extends ContentProvider {
    private static final int NOTE = 1;
    private static final int NOTE_ID = 2;
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private EntertainmentHelper helper;

    static {
        // content://nama app/note
        sUriMatcher.addURI(AUTHORITY,TABLE_NOTE,NOTE);
        // content://nama appp/note/id
        sUriMatcher.addURI(AUTHORITY,TABLE_NOTE+"/#",NOTE_ID);
    }

    @Override
    public boolean onCreate() {
        helper = EntertainmentHelper.getInstance(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings1, String s1) {
        helper.open();
        Cursor cursor;
        switch (sUriMatcher.match(uri)){
            case NOTE:
                cursor = helper.queryProvider();
                break;
            case NOTE_ID:
                cursor = helper.queryByIdProvider(uri.getLastPathSegment());
                break;
                default:
                    cursor= null;
                    break;
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        helper.open();
        long added;
        switch (sUriMatcher.match(uri)){
            case NOTE:
                added = helper.insertProvider(contentValues);
                break;
                default:
                    added = 0;
                    break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI,new DetailActivity.DataObserver(new Handler(),getContext()));
        return Uri.parse(CONTENT_URI+"/"+added);
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        helper.open();
        int deleted;
        switch (sUriMatcher.match(uri)) {
            case NOTE_ID:
                deleted = helper.deleteProvider(uri.getLastPathSegment());
                break;
            default:
                deleted = 0;
                break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI, new DetailActivity.DataObserver(new Handler(), getContext()));
        return deleted;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        helper.open();
        int updated;
        switch (sUriMatcher.match(uri)){
            case NOTE_ID:
                updated = helper.updateProvider(uri.getLastPathSegment(),contentValues);
                break;
                default:
                    updated =0;
                    break;
        }
        getContext().getContentResolver().notifyChange(CONTENT_URI,new DetailActivity.DataObserver(new Handler(),getContext()));
        return updated;
    }
}
