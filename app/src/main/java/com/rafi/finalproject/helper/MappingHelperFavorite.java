package com.rafi.finalproject.helper;

import android.database.Cursor;

import com.rafi.finalproject.entity.EntertainmentEntity;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.DATE;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.DESC;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.IMG;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.RAT;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.TITLE;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.TYPE;
import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.Lang;

public class MappingHelperFavorite {
    public static ArrayList<EntertainmentEntity> mapCursorToArrayList(Cursor cursor, Boolean flag){
        ArrayList<EntertainmentEntity> ent = new ArrayList<>();
        while (cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndexOrThrow(_ID));
            String title = cursor.getString(cursor.getColumnIndexOrThrow(TITLE));
            String date = cursor.getString(cursor.getColumnIndexOrThrow(DATE));
            String desc = cursor.getString(cursor.getColumnIndexOrThrow(DESC));
            float rating = cursor.getFloat(cursor.getColumnIndexOrThrow(RAT));
            String lang = cursor.getString(cursor.getColumnIndexOrThrow(Lang));
            String type = cursor.getString(cursor.getColumnIndexOrThrow(TYPE));
            String img = cursor.getString(cursor.getColumnIndexOrThrow(IMG));
            if(flag){
                if(type.equals("Movie")){
                    ent.add(new EntertainmentEntity(id,title,date,desc,rating,lang,type,img));
                }
            }else {
                if(type.equals("Tv")){
                    ent.add(new EntertainmentEntity(id,title,date,desc,rating,lang,type,img));
                }
            }


        }
        return ent;
    }
}
