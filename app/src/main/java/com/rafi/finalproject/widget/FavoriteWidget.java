package com.rafi.finalproject.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.rafi.finalproject.DetailFavActivity;
import com.rafi.finalproject.R;
import com.rafi.finalproject.entity.EntertainmentEntity;

import java.util.ArrayList;

import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.CONTENT_URI;
import static com.rafi.finalproject.helper.MappingHelper.mapCursorToArrayList;

/**
 * Implementation of App Widget functionality.
 */
public class FavoriteWidget extends AppWidgetProvider {

    private static final String TOAST_ACTION = "com.rafi.finalproject.TOAST_ACTION";
    public static final String EXTRA_ITEM = "com.rafi.finalproject.EXTRA_ITEM";
    private Cursor cursor;

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        Intent intent = new Intent(context,StackWidgetService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,appWidgetId);
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

        RemoteViews views = new RemoteViews(context.getPackageName(),R.layout.favorite_widget);
        views.setRemoteAdapter(R.id.stack_view,intent);
        views.setEmptyView(R.id.stack_view,R.id.empty_view);

        Intent toastIntent = new Intent(context,FavoriteWidget.class);
        toastIntent.setAction(FavoriteWidget.TOAST_ACTION);
        toastIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,appWidgetId);
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,0,toastIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        views.setPendingIntentTemplate(R.id.stack_view,pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId,views);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        cursor = context.getContentResolver().query(CONTENT_URI,null,null,null,null);
        ArrayList<EntertainmentEntity> helper =mapCursorToArrayList(cursor);
        if(intent.getAction() != null){
            if(intent.getAction().equals(TOAST_ACTION)){
                int viewindex = intent.getIntExtra(EXTRA_ITEM,0);
                Intent intent1 = new Intent(context, DetailFavActivity.class);
                intent1.putExtra(DetailFavActivity.DETAIL_FAV,helper.get(viewindex));
                context.startActivity(intent1);
            }
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

