package com.rafi.finalproject.widget;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import com.rafi.finalproject.R;
import com.rafi.finalproject.entity.EntertainmentEntity;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.rafi.finalproject.db.DatabaseContract.EntertainmentColumns.CONTENT_URI;
import static com.rafi.finalproject.helper.MappingHelper.mapCursorToArrayList;


public class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private final List<Bitmap> mWidgetItems = new ArrayList<>();
    private final Context mContext;
    private ArrayList<URL> urls;
    private Cursor cursor;

    @Override
    public void onCreate() {
        urls = new ArrayList<>();
    }

    public StackRemoteViewsFactory(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onDataSetChanged() {
        // db
        if(cursor!=null){
            cursor.close();
        }

        final long identityToken = Binder.clearCallingIdentity();

        cursor = mContext.getContentResolver().query(CONTENT_URI,null,null,null,null);
        ArrayList<EntertainmentEntity> helper =mapCursorToArrayList(cursor);

        Binder.restoreCallingIdentity(identityToken);

        try {
            for(int i=0;i<helper.size();i++){
                urls.add(new URL("https://image.tmdb.org/t/p/original"+helper.get(i).getImg()));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            for(int i=0;i<urls.size();i++){
                mWidgetItems.add(BitmapFactory.decodeStream(urls.get(i).openConnection().getInputStream()));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return mWidgetItems.size();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);
        rv.setImageViewBitmap(R.id.imageView, mWidgetItems.get(i));
        Bundle extras = new Bundle();
        extras.putInt(FavoriteWidget.EXTRA_ITEM, i);
        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);
        rv.setOnClickFillInIntent(R.id.imageView, fillInIntent);
        return rv;
    }


    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
