package com.rafi.finalproject;

import android.database.Cursor;

import com.rafi.finalproject.entity.EntertainmentEntity;

import java.util.ArrayList;

public interface LoadEntCallback {
    void preExecute();
    void postExecute(Cursor cursor);
}
